import React from 'react';
import { StyleSheet, Text, View,Dimensions, } from 'react-native';
import {createDrawerNavigator} from 'react-navigation-drawer'
import {createAppContainer, createSwitchNavigator} from 'react-navigation'
import Dash from './src/Dash/dash'
import Profile from './src/Dash/myProfile'
import FirstAid from './src/Dash/FirstAid'
import signIn from './src/logins/signIn'
import regIn from './src/logins/register'
import SideBar from "./src/Dash/SideBar";
import Donors from './src/Dash/Donors'
import { Ionicons } from "@expo/vector-icons";
import AidInfo from './src/switchScreens/firstAidInfo';

const AppStack = createDrawerNavigator({
  
  dashboard: { 
    screen: Dash,
    navigationOptions: {
      title: "Explore",
      drawerIcon: ({tintColor}) => <Ionicons name="md-home" size={24} color={tintColor}/>
    }
  },
  FirstAid: {
    screen: FirstAid,
    navigationOptions: {
      title: "First Aid",
      drawerIcon: ({tintColor}) => <Ionicons name="md-medkit" size={24} color={tintColor}/>
    }
  },
  Donors: {
    screen: Donors,
    navigationOptions: {
      title: "Donors",
      drawerIcon: ({tintColor}) => <Ionicons name="md-water" size={24} color={tintColor}/>
    }
  },
  profile: {
    screen: Profile,
    navigationOptions: {
      title: "My Profile",
      drawerIcon: ({tintColor}) => <Ionicons name="md-person" size={24} color={tintColor}/>
    }
  },
},


{
  contentComponent: props => <SideBar {...props} />,
  drawerWidth: Dimensions.get("window").width * 0.85,
  hideStatusBar: false,
  contentOptions: {
      activeBackgroundColor: "rgba(212,118,207, 0.2)",
      activeTintColor: "#53115B",
      itemsContainerStyle: {
          marginTop: 16,
          marginHorizontal: 8
      },
      itemStyle: {
          borderRadius: 4
      }
  }
}


//   contentOptions: {
//       activeBackgroundColor: "rgba(212,118,207, 0.2)",
//       activeTintColor: "#53115B",
//       itemsContainerStyle: {
//           marginTop: 16,
//           marginHorizontal: 8
//       },
//       itemStyle: {
//           borderRadius: 4
//       }
//   }
// }
);
export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: signIn,
    RegLoading:regIn,
    Aid:AidInfo,
    App: AppStack,
  },
  {
    initialRouteName: 'App',
  }
));



