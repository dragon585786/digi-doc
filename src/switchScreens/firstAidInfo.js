import React, { Component } from "react";
import { View, Text, StyleSheet, Image, Dimensions } from "react-native";
import SwitchHeader from "../Components/switchHeader";
import { ScrollView } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";


const dimen = Dimensions.get("window").width;

export default class AidInfo extends Component {
  render() {
    const name = this.props.navigation.getParam("name", "some value");
    const des = this.props.navigation.getParam("des", "some value");
    const imgURL = this.props.navigation.getParam("imgURL", "some value");
    const cure = this.props.navigation.getParam("cure", "some value");
    const symptom = this.props.navigation.getParam("symptom", "some value");
    const symptom1 = this.props.navigation.getParam("symptom1", "some value");
    const symptom2 = this.props.navigation.getParam("symptom2", "some value");
    //console.log(imgURL);

    return (
      <View style={{ flex: 1 }}>
        <SwitchHeader navig={this.props.navigation} title={name} />
        <ScrollView style={{ flex: 1 }}>
          <View style={styles.upper}>
            <Image
              source={{
                uri: imgURL
              }}
              style={{
                flex: 1,
                borderRadius: 5,
                height: 200,
                width: dimen - 20
              }}
            />
          </View>

          <View style={styles.mid}>

            <View style={styles.mid1}>
              <View style={styles.mid2}> 
                <Text style={styles.text1}>Description</Text>
              </View>

              <View style={styles.textCont}>
                <Text style={styles.text}><Ionicons name="md-arrow-dropright" size={24} color="black"/> {des}</Text>
              </View>

            </View>



            <View style={styles.mid1}>
              <View style={styles.mid2}>
                <Text style={styles.text1}>Symptoms</Text>
              </View>
              <View style={styles.textCont}>
                <Text style={styles.text}><Ionicons name="md-arrow-dropright" size={24} color="black"/> {symptom}</Text>
                <Text style={styles.text}><Ionicons name="md-arrow-dropright" size={24} color="black"/> {symptom1}</Text>
                <Text style={styles.text}><Ionicons name="md-arrow-dropright" size={24} color="black"/> {symptom2}</Text>
              </View>
            </View>
          </View>



          <View style={styles.bottom}>
          <View style={styles.mid1}>
              <View style={styles.mid2}> 
                <Text style={styles.text1}>Possible Cure</Text>
              </View>

              <View style={styles.textCont}>
                <Text style={styles.text}><Ionicons name="md-arrow-dropright" size={24} color="black"/> {cure}</Text>
              </View>

            </View>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  upper: {
    flex: 1,
    borderRadius: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    elevation: 5
  },
  mid: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    elevation: 5
  },
  bottom: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    elevation: 5
  },
  mid1: {
    marginTop: 10,
    backgroundColor: "#ebe6e6",
    borderRadius: 5,
    borderWidth: 0.1
  },
  mid2: {
    borderBottomWidth: 2,
    borderBottomColor: "white",
    margin: 3,
  },
  textCont:{
    marginLeft:7,
  },
  text: { fontSize: 17, fontWeight: "400" },
  text1:{
    fontSize:19,
    fontWeight:'800',
  }
});

//{ width: 130, height: 130, borderRadius: 75 }
