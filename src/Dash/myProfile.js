import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  Image,
  KeyboardAvoidingView
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import { TouchableOpacity, TextInput } from "react-native-gesture-handler";
import Header from "../Components/header";
import * as firebase from "firebase";
import configFirebase from "../config/firebaseConfig";

export default class Profile extends Component {
  state = {
    photoURL: null,
    displayName: "",
    email: "",
    uid: null,
    isLoggedIn: null,
    password: ""
  };

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        firebase
          .database()
          .ref("users/")
          .child(user.uid)
          .once("value")
          .catch(err => Alert.alert(err.code))
          .then(user => {
            console.log(user);
            this.setState({
              displayName: user.val().displayName,
              email: user.val().email,
              uid: user.val().uid,
              photoURL: user.val().photoURL,
              isLoggedIn: true,
              password: user.val().password
            });
          });
        //console.log(user.uid);
      } else {
        this.setState({
          displayName: "Guest",
          email: "guest@guest.com",
          uid: "gussygussy",
          isLoggedIn: false
        });
      }
    });
  }

  updateUser = async (image, uid, name, email, password) => {
    const res = await fetch(image);
    const blob = await res.blob();
    firebase
      .storage()
      .ref("images/")
      .child(uid)
      .put(blob)
      .catch(err => {
        console.log(err);
      });
    ref = firebase
      .storage()
      .ref("images/")
      .child(uid);
    downloadURL = ref
      .getDownloadURL()
      .then(url => {
        return url;
      })
      .catch(err => console.log(err))
      .then(url => {
        console.log(url);
        firebase
          .database()
          .ref("users/" + uid)
          .update({
            photoURL: url,
            displayName: name,
          })
          .catch(err => console.log(err));
      });
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (name == "") {
      ToastAndroid.showWithGravityAndOffset(
        "name Required",
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    } else if (email == "") {
      ToastAndroid.showWithGravityAndOffset(
        "Email Invalid",
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    } else if (reg.test(email) == false) {
      ToastAndroid.showWithGravityAndOffset(
        "Invalid Email",
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    } else if (password == "") {
      ToastAndroid.showWithGravityAndOffset(
        "Password Required",
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    } else if (password < 6) {
      ToastAndroid.showWithGravityAndOffset(
        "Passowrd must be 6 characters",
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        25,
        50
      );
    }
    var user = firebase.auth().currentUser;
    
    user.updateEmail(email).catch(err => Alert.alert(err.code)).then(
      firebase.database().ref('users/'+ uid).update({
        email:email
      }).catch(err=>console.log(err))
    );
    user.updatePassword(password).catch(err => Alert.alert(err.code)).then(
      firebase.database().ref('users/'+ uid).update({
        password:password
      }).catch(err=>console.log(err))
    );
    Alert.alert("Please Sign In Again");
    this.props.navigation.navigate("AuthLoading");
  };

  pickImage = async () => {
    //this.getPermissionAsync();
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ photoURL: result.uri });
      console.log(this.state.photoURL);
    } else {
      console.log(this.state.photoURL);
    }
  };

  signOut = () => {
    this.setState({
      photoURL: null,
      displayName: null,
      email: null,
      uid: null,
      isLoggedIn: null
    });
    firebase
      .auth()
      .signOut()
      .catch(err => Alert.alert(err.code))
      .then(response => {
        console.log(response);
        this.props.navigation.navigate("AuthLoading");
      });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header title="My Profile" navig={this.props.navigation} />

        <View style={styles.top}>
          <TouchableOpacity onPress={this.pickImage} style={styles.image}>
            {this.state.isLoggedIn ? (
              <Image
                source={{ uri: this.state.photoURL }}
                style={{ width: 130, height: 130, borderRadius: 75 }}
              />
            ) : (
              <Image
                source={require("../../assets/profile-pic.jpg")}
                style={{ width: 130, height: 130, borderRadius: 75 }}
              />
            )}
          </TouchableOpacity>
        </View>

        <KeyboardAvoidingView style={styles.mid} behavior="padding" enabled>
          <View style={styles.textCont}>
            <Text style={styles.text}>Name</Text>
            <TextInput
              style={styles.textInput}
              autoCapitalize="none"
              defaultValue={this.state.displayName}
              onChangeText={displayName => {
                this.setState({ displayName });
              }}
            ></TextInput>
          </View>

          <View style={styles.textCont}>
            <Text style={styles.text}>Email</Text>
            <TextInput
              style={styles.textInput}
              autoCapitalize="none"
              defaultValue={this.state.email}
              onChangeText={email => {
                this.setState({ email });
              }}
            ></TextInput>
          </View>

          <View style={styles.textCont}>
            <Text style={styles.text}>Pass</Text>
            <TextInput
              style={styles.textInput}
              secureTextEntry={true}
              autoCapitalize="none"
              defaultValue={this.state.password}
              onChangeText={password => {
                this.setState({ password });
              }}
            ></TextInput>
          </View>
        </KeyboardAvoidingView>

        <View style={styles.bottom}>
          <View style={{ marginBottom: 20 }}>
            <Button
              title=" Update Profile "
              color="#df42d1"
              onPress={() =>
                this.updateUser(
                  this.state.photoURL,
                  this.state.uid,
                  this.state.displayName,
                  this.state.email,
                  this.state.password
                )
              }
            />
          </View>

          <Button
            title=" Sign Out "
            color="#df42d1"
            onPress={() => this.signOut()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  top: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  mid: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "flex-start",
    marginLeft: 5
  },
  bottom: {
    flex: 1,
    margin: 40
  },
  image: {
    height: 142,
    width: 142,
    borderRadius: 75,
    backgroundColor: "#76216b",
    justifyContent: "center",
    alignItems: "center",
    elevation: 5
  },
  textCont: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10
  },
  text: {
    flex: 0.3,
    fontSize: 20,
    fontWeight: "900"
  },
  textInput: {
    flex: 1,
    padding: 5,
    borderBottomWidth: 2,
    borderColor: "#f7beff",
    borderRadius: 5,
    fontSize: 18,
    marginRight: 10
  }
});
