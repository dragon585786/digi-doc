import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ImageBackground,
  Image,
  StatusBar,
  Alert
} from "react-native";
import { DrawerNavigatorItems } from "react-navigation-drawer";
//import { Ionicons } from "@expo/vector-icons";
import * as firebase from "firebase";
import configFirebase from "../config/firebaseConfig";

export default class SideBar extends Component {
  state = {
    displayName: "",
    photoURL: null,
    isLoggedIn: null
  };

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        firebase
          .database()
          .ref("users/")
          .child(user.uid)
          .once("value")
          .catch(err => Alert.alert(err.code))
          .then(user => {
            this.setState({
              displayName: user.val().displayName,
              photoURL: user.val().photoURL,
              isLoggedIn: true
            });
          });
        //console.log(user.uid);
      } else {
        this.setState({
          displayName: "Guest",
          isLoggedIn: false
        });
      }
    });
  }

  render() {
    return (
      <ScrollView>
        <StatusBar barStyle="default" translucent animated />
        <ImageBackground
          source={require("../../assets/background.png")}
          style={{ width: undefined, padding: 16, paddingTop: 48 }}
        >
          {this.state.isLoggedIn ? (
            <Image
              source={{ uri: this.state.photoURL }}
              style={styles.profile}
            />
          ) : (
            <Image source={require("../../assets/profile-pic.jpg")} style={styles.profile} />
          )}

          <Text style={styles.name}>{this.state.displayName}</Text>
        </ImageBackground>

        <View
          style={styles.container}
          forceInset={{ top: "always", horizontal: "never" }}
        >
          <DrawerNavigatorItems {...this.props} />
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  profile: {
    width: 80,
    height: 80,
    borderRadius: 40,
    borderWidth: 3,
    borderColor: "#FFF"
  },
  name: {
    color: "#FFF",
    fontSize: 20,
    fontWeight: "800",
    marginVertical: 8
  }
});
