import React, { Component } from "react";
import { StyleSheet, Text, View, Button, Alert } from "react-native";
import * as firebase from "firebase";
import configFirebase from "../config/firebaseConfig";
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";
import Header from "../Components/header";
import InfoTab from "../Components/infoTab"

export default class Dash extends Component {
  state = {
    displayName: null,
    email: null
  };

  // getCurrentSignedInUser = () => {
  //   firebase.auth().onAuthStateChanged(user => {
  //     if (user) {
  //       firebase
  //         .database()
  //         .ref("users/")
  //         .child(user.uid)
  //         .once("value")
  //         .catch(err => Alert.alert(error.code))
  //         .then(user => {
  //           console.log(user.val().displayName);
  //           this.setState({
  //             displayName: user.val().displayName,
  //             email: user.val().email
  //           });
  //         });
  //       //console.log(user.uid);
  //     } else {
  //       console.log("not Loggged in ");
  //       this.setState({
  //         displayName: "Guest",
  //         email: "guest@guest.com"
  //       });
  //     }
  //   });
  // };

  render() {
    tabs=[];
    for(i=0;i<=10;i++){
      tabs.push(<InfoTab title={"Title "+i} image={"image"} description={"Some Random DES " + i}/>)
    }
    return (
      <View style={styles.container}>
        <Header title={"Explore"} navig={this.props.navigation}/>
        <ScrollView>
          {tabs}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    justifyContent:'center',
    alignItems:'center',

  }
});
