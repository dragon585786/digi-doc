import React, { Component } from "react";
import { StyleSheet, Text, View, ActivityIndicator } from "react-native";
import Header from "../Components/header";
import DonorTab from "../Components/donorTab";
import { ScrollView } from "react-native-gesture-handler";
import * as firebase from "firebase";
import configFirebase from "../config/firebaseConfig";

export default class Donors extends Component {
  state ={
    donors:null,
  }

  componentDidMount=async()=>{
     await firebase
      .database()
      .ref("donors/")
      .once("value")
      .catch(err => Alert.alert(err))
      .then(res => {
        this.setState({ donors: res.val() })
        //console.log(this.state.donors[1])
      })
      ;
  }

  render() {
    const { donors } = this.state
  
    return (
      <View style={{ flex: 1, alignItems: "center" }}>
        <Header title="Donors" navig={this.props.navigation} />

        <View style={styles.mid}>
          <Text style={styles.text}>BLOOD DONORS IN MUMBAI</Text>
        </View>

        <ScrollView style={{ flex: 1 }}>
          <View style={styles.bottom}>{this.state.donors!=null ? donors.map((user,i)=>{
            return (<DonorTab key={i} title={donors[i].name} blood={donors[i].bloodGroup} number={donors[i].number}/>)
          })
           : <ActivityIndicator size="large" color="#76216b"/> }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mid: {
    flex: 0.1,
    width: 336,
    backgroundColor: "#76216b",
    borderRadius: 5,
    justifyContent: "center"
  },
  text: {
    textAlign: "center",
    color: "white",
    fontSize: 17,
    fontWeight: "900"
  },
  bottom: {
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center"
  }
});
