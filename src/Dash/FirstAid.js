import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ActivityIndicator
} from "react-native";
import Header from "../Components/header";
import FirstAidTab from "../Components/firstAidtabs";
import { ScrollView } from "react-native-gesture-handler";
import * as firebase from "firebase";
import configFirebase from "../config/firebaseConfig";
//import

export default class FirstAid extends Component {
  state = {
    diseases: null
  };

  componentDidMount = async () => {
    await firebase
      .database()
      .ref("firstAid/")
      .once("value")
      .catch(err => Alert.alert(err))
      .then(res => {
        this.setState({ diseases: res.val() });
        //console.log(this.state.donors[1])
      });
  };

  render() {
    const { diseases } = this.state;
    return (
      <View style={styles.container}>
        <Header title="First Aid" navig={this.props.navigation} />
        <ScrollView style={styles.dview}>
          {diseases != null ? (
            diseases.map((user, i) => {
              return (
                <FirstAidTab
                  key={i}
                  nav={this.props.navigation}
                  title={diseases[i].name}
                  des={diseases[i].description}
                  imgURL={diseases[i].imgURL}
                  cure={diseases[i].cure}
                  symptom={diseases[i].symptom}
                  symptom1={diseases[i].symptom1}
                  symptom2={diseases[i].symptom2}
                />
              );
            })
          ) : (
            <ActivityIndicator />
          )}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  dview: {
    flex: 1
  }
});
