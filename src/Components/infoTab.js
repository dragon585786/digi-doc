import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  ActivityIndicator,
  Dimensions
} from "react-native";
import * as firebase from "firebase";
import configFirebase from "../config/firebaseConfig";
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";

const dimen = Dimensions.get("window").width
const high = Dimensions.get("window").height

export default class InfoTab extends Component {
  render() {
    return (
      <View style={styles.container}>
        

          <TouchableOpacity style={styles.card}>

            <View style={styles.img}>
              <ActivityIndicator size="large" color="#76216b" />
              <Text> {this.props.image}</Text>
            </View>

            <View style={styles.textDes}>
              <Text style={styles.text}>{this.props.title}</Text>
              <Text style={styles.description}>{this.props.description}</Text>
            </View>

          </TouchableOpacity>

        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginBottom:10,
  },
  card: {
    justifyContent: "center",
    flex: 1,
    backgroundColor: "#f7beff",
    height: high/2.6-20,
    width: dimen-20,
    borderRadius: 8,
    marginBottom: 10,
    elevation: 5,
  },
  text: {
    fontSize: 20,
    fontWeight: "900",
    textAlign: "left"
  },
  description: {
      
  },
  img: {
    flex: 1,
    borderBottomWidth: 2,
    borderColor: "white",
    alignItems: "center",
    justifyContent: "center"
  },
  textDes: {
    flex: 0.35,
    marginLeft: 8
  }
});
