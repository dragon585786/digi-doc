import React, { Component } from "react";
import { StyleSheet, Text, View, Dimensions} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";

const dimen = Dimensions.get("window").width

export default class FirstAidTab extends Component {

  navigate = () =>{
    this.props.nav.navigate('FirstAid')
    console.log('called')
  }
  
  render() {

    //const gg="good game";
    var name = this.props.title;
    var des = this.props.des;
    var imgURL = this.props.imgURL;
    var cure = this.props.cure;
    var symptom = this.props.symptom;
    var symptom1 = this.props.symptom1;
    var symptom2 = this.props.symptom2;  

    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.card} onPress={()=>this.props.nav.navigate('Aid',{
          name:name,
          des:des,
          imgURL:imgURL,
          cure:cure,
          symptom:symptom,
          symptom1:symptom1,
          symptom2:symptom2,
        })}>
          <View style={{flex: 1,justifyContent:'center'}}>
            <Text style={styles.text}>{this.props.title}</Text>
          </View>

          <View
            style={{
              flex: 0.3,
              alignItems: "flex-end",
              justifyContent: "center",
              paddingRight: 10
            }}
          >
            <Ionicons name="ios-arrow-forward" size={29} color={"white"} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
    //alignItems:'center',
  },
  card: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#76216b",
    height: 59,
    width: dimen-20,
    margin: 10,
    marginTop:0,
    borderRadius: 10,
    justifyContent: "center"
  },
  text: {
    fontSize: 25,
    fontWeight: "900",
    color: "white",
    alignItems: "flex-start",
    paddingLeft: 10,
  }
});
