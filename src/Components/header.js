import React, { Component } from "react";
import { View, Text, StyleSheet, StatusBar,Dimensions } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import {DrawerActions} from 'react-navigation-drawer'

const high = Dimensions.get("window").height

export default class Header extends Component {

    open = () => {
        console.log("OPening")
        this.props.navigation.dispatch(DrawerActions.openDrawer)
        console.log("OPening")
    }

  render() {
    return (
      <View style={style.container}>
        <View style={{ paddingTop: Expo.Constants.statusBarHeight }}></View>
        <StatusBar barStyle="default" />
        
        <Ionicons
          style={style.icons}
          name="md-menu"
          size={29}
          onPress={()=>this.props.navig.openDrawer()}
          color="#fff"
        />
        <Text style={style.text}>{this.props.title}</Text>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    //flex:1,
    backgroundColor: "#76216b",
    elevation: 7,
    height: high/9.5,
    flexDirection: 'row',
    marginBottom:10,
  },
  text: {
    flex:1,
    alignItems:'flex-start',
    fontSize:22,
    color: "#fff",
    fontWeight: "900",
    marginTop :Expo.Constants.statusBarHeight + 7
  },
  icons: {
    marginLeft: 10,
    flex:0.13,
    marginTop :Expo.Constants.statusBarHeight + 7
    //paddingRight:50,
  }
});
