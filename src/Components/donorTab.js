import React, { Component } from "react";
import { StyleSheet, Text, View,Dimensions } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";

const dimen = Dimensions.get("window").width

export default class DonorTab extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.card}>
          <View style={styles.upper}>
            <Ionicons name="ios-person" size={69} />
          </View>
          <View style={styles.lower}>
            <Text >{this.props.title}</Text>
            <Text >No : {this.props.number}</Text>
            <Text >Blood Group : {this.props.blood} </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    //flex: 1
    margin:10
  },
  card: {
    backgroundColor: "#ebe6e6",
    height: 220,
    width: 145,
    borderRadius: 5,
    borderWidth: 0.1,
    alignItems: "center"
  },
  upper:{
    flex:1,
    justifyContent:'center',
},
lower:{
  flex:1,
  justifyContent:'flex-start',
}
});
